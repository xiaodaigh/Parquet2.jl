```@meta
CurrentModule = Parquet2
```

# API

```@index
```

## Basic Usage
```@docs
Dataset
RowGroup
Column
load
```

## Schema and Introspection
```@docs
SchemaNode
PartitionNode
Page
ColumnStatistics

parqtype
juliatype
juliamissingtype
nvalues
iscompressed
isdictencoded
metadata
pages
```

## Internals
See [Internals](@ref internals_api).
