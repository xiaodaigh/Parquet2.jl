# TODO
The following are priorities for future development:

- **Writing!**  This can't write anything at all yet.
- Full unit testing!  Right now we are just validating on complete datasets.
- Hive partition schema inference without metadata.  (Very large parquet sets tend not to have
    top-level metadata).
- Filtering and selection based on partition schema (at least for the hive schema, it's not really
    specified in the general case).
- Other compression types (very simple to add for anything that already has methods in Julia,
    otherwise it's a matter of wrapping C libs).
- [Column indexing and page skipping](https://github.com/apache/parquet-format/blob/master/PageIndex.md).
    Not really availble in many other writers it would seem, but could be very useful when reading
    and writing using this package.


## Low Priority
The following are wanted but are considered lower priority.

- Better tools for dealing with custom metadata.
- Nested column types (list, map).
- Column encryption.
- More remote file system types (e.g. HDFS).  For the most part, it should be possible to add these
    without touching this package.
