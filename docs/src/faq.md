
# FAQ

## Which format should I use?
First off, parquet is *very* much a *tabular* format.  If the data you are looking to store is not
explicitly tabular, you don't want to use parquet.  See alternatives such as
[HDF5.jl](https://github.com/JuliaIO/HDF5.jl), [JLD2.jl](https://github.com/JuliaIO/JLD2.jl),
[LightBSON.jl](https://github.com/ancapdev/LightBSON.jl) or
[UnROOT.jl](https://github.com/tamasgal/UnROOT.jl).

If your format is tabular, you *probably* want to use [Arrow.jl](https://github.com/JuliaData/Arrow.jl).
The reason is that the arrow format is much closer to a natural in-memory format and as such will
be much less computationally expensive and require far fewer extra allocations than parquet in most
cases.

Parquet is best for large tabular datasets (``\gtrsim 10~\textrm{GB}``) where some level of compression
is desirable and very often the datasets are partitioned into multiple separate files.  In some
cases, the more efficient data storage comes at a high cost in terms of CPU usage and extra memory
allocations.  For example, null values are still present (as arbitrary bytes) in the underlying data
buffer.  In parquet they are skipped, meaning that the locations of individual elements in nullable
parquet columns are not knowable *a priori*.  A consequence of this is that reading nullable values
from parquet will always be significantly less efficient than loading the same values from arrow,
but the parquet itself will be smaller, particularly if there is a large number of nulls.

Parquet has a slight advantage over arrow in that the data schema is substantially simpler in most
cases.

Of course, the most likely reason you will have to use parquet as opposed to arrow is because you
have been provided a parquet and simply have no say in the matter.  Parquet is arguably the most
ubiquitous binary format in "big data" applications and is often output by unpleasant but commonly
used (particularly JVM-based) programs in this domain such as apache spark.

**TL;DR:** Don't use parquet if not explicitly tabular.  If tabular, you are probably better off
with [Arrow.jl](https://github.com/JuliaData/Arrow.jl).  If you have large quantities of data or
have been given a parquet, use parquet.

## Why start from scratch instead of improving [Parquet.jl](https://github.com/JuliaIO/Parquet.jl)?
Most of my most wanted features would have required (at least) a major re-write of the existing
Parquet.jl, such as:
- Maximally lazy loading of data.
- Flexibility to load from alternate file systems such as S3.
- Progressive, cached loading from remote file systems.
- An API that is more decomposable into the basic schema components.
- Easier user interface with features such as `RowGroup`s that are full Tables.jl tables.

There really would not have been much left of the original package by the time I was through.
