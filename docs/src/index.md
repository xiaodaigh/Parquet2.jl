```@meta
CurrentModule = Parquet2
```

# Parquet2

This package implements the [parquet tabular data storage
format](https://github.com/apache/parquet-format) in pure Julia.  A particular emphasis is placed on
lazy loading, schema introspection and the flexibility to load data from a variety of sources such
as S3 or min.io.  The package is based on a lazy data loading and caching system that allows users
to load only the needed subsets of data rather than entire files.  This is particularly important
for the parquet format since it is often used to store "large" quantities of data (i.e. much larger
than is likely to fit in memory).

Parquet data loaded by Parquet2.jl is organized analogously to the underlying binary data schema:
```
Dataset (← Tables.jl table)
    ⎸ RowGroup (← Tables.jl table)
    ⎸   ⎸ Column1 (← lazy AbstractVector)
    ⎸   ⎸ Column2 (← lazy AbstractVector)
    ⎸   ⎸ ⋮
    ⎸   ⎸ ColumnN (← lazy AbstractVector)
    ⎸ RowGroup2 (← Tables.jl table)
    ⎸ ⋮
    ⎸ RowGroupN (← Tables.jl table)
    ⎸   ⋮
```
- [`Dataset`](@ref): a [Tables.jl table](https://tables.juliadata.org/dev/) and an indexable set of
    `RowGroup`.
- [`RowGroup`](@ref): a Tables.jl table and an indexable (by string name or integer) set of
    `Column`.
- [`Column`](@ref): an abstraction for getting `AbstractVector` objects which lazily construct data
    where possible.

Parquet files are divided into ``n > 0`` `RowGroup`s each of which represents a contiguous
subset of rows in the full table.  `RowGroup`s typically correspond to files.  Parquet2.jl
treats the `RowGroup`s as full tables in-and-of themselves and they can be loaded just like the full
dataset.

!!! note

    Writing is not yet supported!  See [TODO](@ref).

## Usage

### Loading a Dataset
```julia
using FilePathsBase
using Parquet2: Dataset

ds = Dataset("/path/to/file")  # this only loads metadata
ds = Dataset(p"s3://path/to/file")  # can understand path types (may require extension modules)
ds = Dataset("/path/to/dir.parquet/")  # parquets may be a directory that contain top-level metdata
```

Showing the dataset will display the schema, here is one of the validation datasets:
```julia
◖◗ ds = Dataset("test/data/std_fastparquet.parq")
≔ Dataset (31016 bytes)
	1. "floats": Union{Missing, Float64}
	2. "floats_missing": Union{Missing, Float64}
	3. "ints": Union{Missing, Int64}
	4. "ints_missing": Union{Missing, Float64}
	5. "strings": Union{Missing, String}
	6. "strings_missing": Union{Missing, String}
	7. "timestamps": Union{Missing, DateTime}
	8. "timestamps_missing": Union{Missing, DateTime}
	9. "missings": Missing
	10. "dictionary": Union{Missing, Int64}
	11. "dictionary_missing": Union{Missing, Int64}
```

The dataset is an indexable, iterable collection of `RowGroup` objects
```julia
length(ds)  # gives the number of row groups

rg = ds[1]  # first row group


◖◗ for rg ∈ ds
        println(rg)
    end
≔ RowGroup (14486 bytes) (250 rows)
	1. "floats": Union{Missing, Float64}
	2. "floats_missing": Union{Missing, Float64}
	3. "ints": Union{Missing, Int64}
	4. "ints_missing": Union{Missing, Float64}
	5. "strings": Union{Missing, String}
	6. "strings_missing": Union{Missing, String}
	7. "timestamps": Union{Missing, DateTime}
	8. "timestamps_missing": Union{Missing, DateTime}
	9. "missings": Missing
	10. "dictionary": Union{Missing, Int64}
	11. "dictionary_missing": Union{Missing, Int64}

≔ RowGroup (14480 bytes) (250 rows)
	1. "floats": Union{Missing, Float64}
	2. "floats_missing": Union{Missing, Float64}
	3. "ints": Union{Missing, Int64}
	4. "ints_missing": Union{Missing, Float64}
	5. "strings": Union{Missing, String}
	6. "strings_missing": Union{Missing, String}
	7. "timestamps": Union{Missing, DateTime}
	8. "timestamps_missing": Union{Missing, DateTime}
	9. "missings": Missing
	10. "dictionary": Union{Missing, Int64}
	11. "dictionary_missing": Union{Missing, Int64}
```

Note that each `RowGroup` matches the schema of the `Dataset`.  They may not have the same number of
rows.


## Accessing Data
[`Dataset`](@ref) and [`RowGroup`](@ref) both satisfy the
[Tables.jl](https://github.com/JuliaData/Tables.jl) columnar table interface and can therefore be
easily converted to tables.
```julia
using Tables, DataFrames

using Parquet2: Dataset

ds = Dataset("/path/to/file")

sch = Tables.schema(ds)  # get the Tables.jl Schema object

c = Tables.getcolumn(ds, :col1)  # load *only* col1; others are not touched

c = Parquet2.load(ds, "col1")  # equivalent to the above


df = DataFrame(ds)  # load the entire table as a DataFrame

df1 = DataFrame(ds[1])  # load the first RowGroup as a DataFrame
```


### Using TableOperations.jl to load Specific Columns
This package supports loading of specific columns leaving the rest untouched.  With the exception of
the top-level metadata, only data from selected columns is loaded.

While columns can easily be loaded individually with `Tables.getcolumn` or [`Parquet2.load`](@ref),
it is more convenient to select a subset of them using
[TableOperations.jl](https://github.com/JuliaData/TableOperations.jl).  In particular, see
[`TableOperations.select`](https://github.com/JuliaData/TableOperations.jl#tableoperationsselect).

For example
```julia
using TableOperations; const TO = TableOperations

ds = Parquet2.Dataset("/path/to/file")

df = ds |> TO.select(:col1, :col2) |> DataFrame
```

## Extensions
The following packages are available for loading data from alternative (i.e. not the local
file-syste) sources:
- [ParquetS3.jl](https://gitlab.com/ExpandingMan/ParquetS3.jl)
