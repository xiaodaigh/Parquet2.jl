
"""
    Dataset <: ParquetTable

A complete parquet dataset created from top-level parquet metadata.  Each `Dataset` is an indexable collection of
[`RowGroup`](@ref)s each of which is a Tables.jl compatible columnar table in its own right.  The
`Dataset` is a [Tables.jl compatible](https://tables.juliadata.org/dev/) columnar table consisting of (lazily
by default) concatenated `RowGroup`s.  A `Dataset` can consist of data in any number of files, but it is unaware
of files which are not referred to by the metadata regardless of directory structure.

## Constructors
```julia
Dataset(fm::FileManager; support_legacy=true)
Dataset(p::AbstractPath; use_mmap=true, subset_length=_depends_on_path_type, max_subsets=typemax(Int), preload=true)
Dataset(v::AbstractVector{UInt8}; kw...)
Dataset(io::IO; kw...)
Dataset(str::AbstractString; kw...)
```

## Arguments
- `fm`: A [`FileManager`](@ref) object describing a set of files to be loaded.
- `p`: Path to main metadata file or directory containing a `_metadata` file.  Loading behavior will depend on
    the type of path provided.
- `v`: An in-memory (or memory mapped) byte buffer.
- `io`: An `IO` object from which data can be loaded.
- `str`: File or directory path as a string.  Converted to `AbstractPath` with `Path(str)`.

## Usage
```julia
ds = Dataset("/path/to/parquet")
ds = Dataset(p"s3://path/to/parquet")  # understands different path types

length(ds)  # gives number of row groups

rg = ds[1]  # index to get row groups

for rg ∈ ds  # is an indexable, iterable collection of row groups
    println(rg)
end

df = DataFrame(ds)  # Tables.jl compatible, is concatenation of all row groups

# use TableOperations.jl to load only selected columns
df = ds |> TableOperations.select(:col1, :col2) |> DataFrame
```
"""
struct Dataset{ℱ<:FileManager} <: ParquetTable
    file_manager::ℱ
    meta_orig::Meta.FileMetaData
    schema::SchemaNode
    row_groups::Vector{RowGroup}
    name_index::NameIndex
    partition_tree::PartitionNode
    partition_column_names::OrderedSet{String}
    metadata::Dict{String,Any}
end

function Dataset(fm::FileManager; support_legacy::Bool=true)
    v = get(fm)
    isparquet(v) || error("invalid parquet Dataset $p")
    m = readmeta(v)
    r = SchemaNode(m.schema; support_legacy)
    ptree = PartitionNode(fm)
    pnames = OrderedSet(columnnames(ptree))
    rgs = m.row_groups |> Map(rg -> RowGroup(fm, r, rg, ptree)) |> collect
    nidx = (pnames, children(r) |> Map(name)) |> Cat() |> collect |> NameIndex
    Dataset{typeof(fm)}(fm, m, r, rgs, nidx, ptree, pnames, unpack_thrift_metadata(m))
end

function Dataset(p::AbstractPath; use_mmap::Bool=is_mmapable_path(p), kw...)
    if isdir(p)
        p = joinpath(p, "_metadata")
        isfile(p) || throw(ArgumentError("directory $(dirname(p)) does not contain a parquet metadata file"))
    elseif !isfile(p)
        throw(ArgumentError("$p does not exist"))
    end
    Dataset(FileManager(p; use_mmap); kw...)
end
Dataset(p::AbstractString; kw...) = Dataset(AbstractPath(p); kw...)
Dataset(io::IO; kw...) = Dataset(FileManager(read(io)); kw...)
Dataset(v::AbstractVector{UInt8}; kw...) = Dataset(FileManager(v); kw...)

FileManager(ds::Dataset) = ds.file_manager

"""
    dirname(ds::Dataset)

Get the parent directory of the dataset.
"""
Base.dirname(ds::Dataset) = dirname(ds.file_manager)

partition_column_names(ds::Dataset) = ds.partition_column_names

"""
    pathof(ds::Parquet2.Dataset)

Get the path of the file containing the main metadata for dataset `ds`.
"""
Base.pathof(ds::Dataset) = isnothing(ds.path) ? nothing : ds.path

nbytes(ds::Dataset) = ds |> FileManager |> get |> length

_min_parquet_size() = 2*length(MAGIC) + FOOTER_LENGTH

function isparquet(v::CacheVector)
    isempty(v) && return false
    v[1:length(MAGIC)] == MAGIC && length(v) ≥ _min_parquet_size()
end

function readmetalength(v::CacheVector)
    j = length(v) - length(MAGIC) - FOOTER_LENGTH + 1
    only(reinterpret(Int32, v[j:(j+3)]))
end
readmetalength(ds::Dataset) = readmetalength(ds.metadata_fetcher)

function readmeta(v::CacheVector)
    ℓ = readmetalength(v)
    a = length(v) - length(MAGIC) - FOOTER_LENGTH - ℓ
    m = readthrift(CacheIO(v, a), Meta.FileMetaData)
    @debug("read parquet Dataset metadata")
    m
end

"""
    metadata(ds::Dataset)

Get the auxiliary key-value metadata for the dataset.
"""
metadata(ds::Dataset) = ds.metadata

"""
    metadata(ds::Dataset, k::AbstractString, default=nothing)

Get the key `k` from the key-value metadata for the dataset, returning `default` if not present
"""
metadata(ds::Dataset, k::AbstractString, default=nothing) = get(metadata(ds), k, default)

rowgroups(ds::Dataset) = ds.row_groups

nrowgroups(ds::Dataset) = length(ds.row_groups)

DataAPI.nrow(ds::Dataset) = sum(nrow, rowgroups(ds))

RowGroup(ds::Dataset, n::Integer) = rowgroups(ds)[n]

Base.length(ds::Dataset) = nrowgroups(ds)

Base.getindex(ds::Dataset, n::Integer) = RowGroup(ds, n)

Base.close(ds::Dataset) = close(ds.file_manager)

function Base.iterate(ds::Dataset, n::Integer=1)
    n > nrowgroups(ds) && return nothing
    RowGroup(ds, n), n+1
end

Column(ds::Dataset, r::Integer, c::Union{Integer,AbstractString}) = Column(RowGroup(ds, r), c)

function PageLoader(ds::Dataset, r::Integer, c::Union{Integer,AbstractString}, p::Integer=1)
    PageLoader(ds.schema, Column(ds, r, c), p)
end

"""
    load(ds::Dataset, n)

Load the (complete, all `RowGroup`s) column `n` (integer or string) from the dataset.
"""
function load(ds::Dataset, n::Union{Integer,AbstractString})
    if nrowgroups(ds) == 0
        Vector{juliamissingtype(ds, n)}()
    elseif nrowgroups(ds) == 1
        load(RowGroup(ds, 1), n)
    else
        Vcat((load(rg, n) for rg ∈ rowgroups(ds))...)
    end
end

Tables.partitions(ds::Dataset) = rowgroups(ds)
