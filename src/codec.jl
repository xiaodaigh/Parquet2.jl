
"""
    bitmask(𝒯, α, β)
    bitmask(𝒯, β)

Create a bit mask of type `𝒯 <: Integer` where bits `α` to `β` (inclusive) are `1` and the rest are `0`, where
bit `1` is the least significant bit.  If only one argument is given it is taken as the *end* position `β`.
"""
function bitmask(::Type{𝒯}, α::Integer, β::Integer) where {𝒯<:Integer}
    o = zero(𝒯)
    for k ∈ α:β
        o += 𝒯(2)^(k-1)
    end
    o
end
bitmask(::Type{𝒯}, β::Integer) where {𝒯} = bitmask(𝒯, 1, β)

"""
    bitwidth(n::Integer)

Compute the width in bits needed to encode integer `n`, truncating leading zeros.  For example, `1` has a width of `1`,
`3` has a width of `2`, `8` has a width of `4`, et cetera.
"""
bitwidth(n::Integer) = ceil(Int, log(2, n+1))

"""
    bytewidth(n::Integer)

Compute the width in bytes needed to encode integer `n` truncating leading zeros beyond the nearest byte boundary.
For example, anything expressible as a `UInt8` has a byte width of `1`, anything expressible as a `UInt16` has a
byte width of `2`, et cetera.
"""
bytewidth(n::Integer) = cld(bitwidth(n), 8)

"""
    BitUnpackVector{𝒯}

A vector type that unpacks underlying data into values of type `𝒯` when indexed.
"""
struct BitUnpackVector{𝒯,𝒱<:AbstractArray{UInt8}} <: AbstractVector{𝒯}
    data::𝒱
    width::Int
    length::Int
end

function BitUnpackVector{𝒯}(v::AbstractVector{UInt8}, width::Integer, ℓ::Integer=fld(8*length(v), width)) where {𝒯}
    BitUnpackVector{𝒯,typeof(v)}(v, width, ℓ)
end

Base.size(v::BitUnpackVector) = (v.length,)

Base.IndexStyle(::Type{<:BitUnpackVector}) = IndexLinear()

function Base.getindex(v::BitUnpackVector{𝒯}, i::Int) where {𝒯}
    a₀ = fld((i-1)*v.width, 8) + 1
    a₁ = fld1(i*v.width, 8)
    b₀ = mod((i-1)*v.width, 8) + 1
    b₁ = mod1(i*v.width, 8)
    x = zero(𝒯)
    if a₀ == a₁
        m = bitmask(UInt8, b₀, b₁)
        return x | ((m & v.data[a₀]) >> (b₀ - 1))
    end
    for (j, α) ∈ enumerate(a₀:a₁)
        m, δ = if α == a₀
            bitmask(UInt8, b₀, 8), b₀-1
        elseif α == a₁
            bitmask(UInt8, 1, b₁), b₁
        else
            0xff, 0
        end
        x = x | (𝒯(m & v.data[α]) << ((j-1)*v.width - δ))
    end
    x
end


"""
    leb128encode(n::Unsigned)
    leb128encode(io::IO, n::Unsigned)

Encode the integer `n` as a byte array according to the
[LEB128](https://en.wikipedia.org/wiki/LEB128) encoding scheme.
"""
function leb128encode(n::Unsigned)
    ℓ = 8*sizeof(n)
    o = UInt8[]
    while !iszero(n)
        b = UInt8(0x7f & n)
        n = n >> 7
        iszero(n) || (b = b | 0x80)
        push!(o, b)
    end
    o
end
function leb128encode(io::IO, n::Unsigned)
    ℓ = 8*sizeof(n)
    o = 0
    while !iszero(n)
        b = UInt8(0x7f & n)
        n = n >> 7
        iszero(n) || (b = b | 0x80)
        o += write(io, b)
    end
    o
end

"""
    leb128decode(𝒯, v)
    leb128decode(𝒯, io)

Decode `v` to an integer of type `𝒯 <: Unsigned` according to the
[LEB128](https://en.wikipedia.org/wiki/LEB128) encoding scheme.
"""
function leb128decode(::Type{𝒯}, v::AbstractVector{UInt8}) where {𝒯<:Unsigned}
    o = zero(𝒯)
    δ = 0
    for b ∈ v
        o = o | ((𝒯(0x7f) & b) << δ)
        (0x80 & b) == 0 && break
        δ += 7
    end
    o
end
function leb128decode(::Type{𝒯}, io::IO) where {𝒯<:Unsigned}
    o = zero(𝒯)
    δ = 0
    while !eof(io)
        b = read(io, UInt8)
        o = o | ((𝒯(0x7f) & b) << δ)
        (0x80 & b) == 0 && break
        δ += 7
    end
    o
end

"""
    readfixed(io, 𝒯, N, v=zero(𝒯))
    readfixed(w::AbstractVector{UInt8}, 𝒯, N, i=1, v=zero(𝒯))

Read a `𝒯 <: Integer` from the first `N` bytes of `io`.  This is for reading integers which have had
their leading zeros truncated.
"""
function readfixed(io::IO, ::Type{𝒯}, N::Integer, v::𝒯=zero(𝒯)) where {𝒯<:Integer}
    for n ∈ 0:(N-1)
        b = convert(𝒯, read(io, UInt8))
        v = v | (b << 8n)
    end
    v
end
function readfixed(w::AbstractArray{UInt8}, ::Type{𝒯}, N::Integer, i::Integer=1, v::𝒯=zero(𝒯)) where {𝒯<:Integer}
    for n ∈ 0:(N-1)
        b = convert(𝒯, w[i])
        i += 1
        v = v | (b << 8n)
    end
    v
end

"""
    decodebitpacked(io, 𝒯, η, w, ℓ=fld(8*cld(η, 8), w))

Read `η` bits from `io` and decode bit-packed data of type `𝒯` with bit-width `w` and `ℓ` values.
"""
function decodebitpacked(io::IO, ::Type{𝒯}, η::Integer, w::Integer, ℓ::Integer=fld(8*cld(η, 8), w)) where {𝒯}
    BitUnpackVector{𝒯}(read(io, cld(η, 8)), w, ℓ)
end

"""
    decoderle1(io::IO, 𝒯, w, η)

Decode a single run of `η` run-length-encoded values from `io` with bit width `w`.
`𝒯` is the type of the decoded values.
"""
decoderle1(io::IO, ::Type{𝒯}, w::Integer, η::Integer) where {𝒯} = Fill(readfixed(io, 𝒯, cld(w, 8)), η)

"""
    decoderle1!(v, io, w, η)

Decode a single run of `η` run-lenght-encoded value from `io` with bit width `w` and store them in `v`.
Note this fills *all* elemsnts of `v` so it should be appropriately sized.
"""
function decoderle1!(v::AbstractVector{𝒯}, io::IO, w::Integer, η::Integer) where {𝒯}
    v .= readfixed(io, 𝒯, cld(w, 8))
end

#====================================================================================================
       NOTE:

`decodehybrid` is pretty tricky because it is very difficult to determine how many bytes to read
for each run.  In principle the spec allows for arbitrarily many separate bit-packed runs but
it seems unlikely that these will ever occur.  In the bit-packed case, the number of values
is stored in multiples of 8.

There are several tricks here to make this (hopefully) reliable.  They may not all be necessary
but it seems worth the tiny cost in performance
- Check if hit eof
- Check if the number of values read excedes the number of values indicated by metadata
- Check if we have surpassed the number of bytes read by metadata
- Make sure `h` (which gives number of values to read in a run) gives `0`.  If it does,
    it's likely an indiation that we hit padding. This shouldn't happen.
====================================================================================================#

"""
    decodehybrid(io::IO, 𝒯, w, ℓ; nbytes=Int(read(io, UInt32)))

Decode an array of element type `𝒯` from `io` according to parquet's run-length-encoded/bit-packed
hybrid encoding scheme as described
[here](https://github.com/apache/parquet-format/blob/master/Encodings.md#run-length-encoding--bit-packing-hybrid-rle--3).

## Arguments
- `𝒯`: the element type of the decoded array.
- `io`: the IO stream to read from.
- `w`: the width (in bits) of elements read from either the run-length or bit-pack encoding.
- `ℓ`: the number of values in the resulting array.
- `nbytes`: the total number of bytes of the hybrid encoded data, excluding this number.  By default,
    this contained in the first 4 bytes of the encoding.
"""
function decodehybrid(io::IO, ::Type{𝒯}, w::Integer, ℓ::Integer;
                      nbytes::Integer=Int(read(io, UInt32)),
                     ) where {𝒯}
    p₀ = position(io)
    vs = []
    λ = 0  # number of values read so far
    while !eof(io) && λ < ℓ && position(io) - p₀ < nbytes
        h = Int(leb128decode(UInt32, io))
        h == 0 && break  # shouldn't happen, but maybe if you hit padding
        if iseven(h)
            v = decoderle1(io, 𝒯, w, min(h >> 1, ℓ - λ))
            λ += length(v)
            push!(vs, v)
        else
            h = h >> 1
            # this gives the number of bits to read; the factor of 8 is part of the spec
            η = 8h*w
            v = decodebitpacked(io, 𝒯, η, w, min(ℓ - λ, 8h))
            λ += length(v)
            push!(vs, v)
        end
    end
    # return types seem reasonable, but this still makes me a little nervous...
    Vcat(vs...)
end

function decodehybrid!(v::AbstractVector{𝒯}, io::IO, w::Integer, ℓ::Integer;
                       nbytes::Integer=Int(read(io, UInt32)),
                      ) where {𝒯}
    p₀ = position(io)
    λ = 0  # number of values read so far
    while !eof(io) && λ < ℓ && position(io) - p₀ < nbytes
        h = Int(leb128decode(UInt32, io))
        h == 0 && break
        if iseven(h)
            η = min(h >> 1, ℓ - λ)
            decoderle1!(view(v, (λ+1):(λ+η)), io, w, η)
            λ += η
        else
            h = h >> 1
            η = 8h*w
            # this looks like an extra allocation but actually `w` is lazy so it shouldn't be too bad
            u = decodebitpacked(io, 𝒯, η, w, min(ℓ - λ, 8h))
            view(v, (λ+1):(λ+length(u))) .= u
            λ += length(u)
        end
    end
    v
end

function decodehybrid(::Type{Vector{𝒯}}, io::IO, w::Integer, ℓ::Integer;
                      nbytes::Integer=Int(read(io, UInt32))) where {𝒯}
    v = Vector{𝒯}(undef, ℓ)
    decodehybrid!(v, io, w, ℓ; nbytes)
end
