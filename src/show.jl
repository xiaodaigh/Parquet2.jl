
function Base.show(io::IO, ::MIME"text/plain", v::CacheVector)
    show(io, typeof(v))
    print(io, " of length ", length(v), "; ")
    print(io, " (subset length: ", v.subset_length, ") ")
    print(io, " (subsets loaded: ", ncached(v), ") ")
    isempty(v.cache) || print(io, " (bytes loaded: ", values(v.cache) |> Map(length) |> sum, ")")
    nothing
end
Base.show(io::IO, v::CacheVector) = show(io, MIME("text/plain"), v)


show_nbytes(io::IO, n::Integer) = print(io, "(", n, " bytes)")

function show_schema_row(io::IO, n::Integer, name, type)
    print(io, "\t$n. ")
    printstyled(io, "\"", string(name), "\"", color=:yellow)
    print(io, ": ")
    printstyled(io, string(type), color=:cyan)
    print(io, "\n")
end

function Base.show(io::IO, t::ParquetTable)
    printstyled(io, "≔ ", color=:blue)
    str = split(sprint(show, typeof(t)), "{")[1]
    print(io, str, " ")
    show_nbytes(io, nbytes(t))
    isnrowsknown(t) && printstyled(io, " (", nrow(t), " rows)", color=:green)
    print(io, "\n")
    sch = Tables.schema(t)
    if isempty(sch.names)
        printstyled(io, "\t[no columns]\n", color=:red)
    else
        foreach(tpl -> show_schema_row(io, tpl...), zip(1:length(sch.names), sch.names, sch.types))
    end
end

function Base.show(io::IO, c::Column)
    printstyled(io, "⫶ ", color=:blue)
    print(io, typeof(c), " ")
    show_nbytes(io, nbytes(c))
    if c.compression_codec ≠ :uncompressed
        print(io, " ($(c.compression_codec) compressed) ")
    end
    printstyled(io, " (", nvalues(c), " rows)", color=:green)
    print(io, "\n\t")
    if isempty(c.pages)
        printstyled(io, "[pages not loaded]")
    else
        printstyled(io, "[$(length(c.pages)) pages]", color=:magenta)
        isdictencoded(c) && print(io, "  (dict encoded)")
    end
    print(io, "\n")
end
