
"""
    getcodecname(n::Integer)

Get the name (`Symbol`) of the codec corresponding to the integer `n` in the parquet metadata schema.
"""
getcodecname(n::Integer) = thriftenum(Meta.CompressionCodec, n)

"""
    getcompressor(s::Symobl)

Get the function `𝒻(::AbstractVector{UInt8})::AbstractVector{UInt8}` for compressing data to codec `s`.
"""
getcompressor(s::Symbol) = getcompressor(Val{s})
getcompressor(n::Integer) = getcompressor(getcodecname(n))

"""
    getdecompressor(s::Symbol)

Get the function `𝒻(::AbstractVector{UInt8})::AbstractVector{UInt8}` for decompressing data from codec `s`.
"""
getdecompressor(s::Symbol) = getdecompressor(Val{s})
getdecompressor(n::Integer) = getdecompressor(getcodecname(n))


getcompressor(::Type{s}) where {s} = throw(ArgumentError("unknown compression codec $s"))
getdecompressor(::Type{s}) where {s} = throw(ArgumentError("unknown compression codec $s"))

getcompressor(::Type{Val{:uncompressed}}) = identity

getdecompressor(::Type{Val{:uncompressed}}) = identity

getcompressor(::Type{Val{:snappy}}) = Snappy.compress ∘ Vector
getdecompressor(::Type{Val{:snappy}}) = Snappy.uncompress ∘ Vector

getcompressor(::Type{Val{:gzip}}) = v -> transcode(GzipCompressor, Vector(v))
getdecompressor(::Type{Val{:gzip}}) = v -> transcode(GzipDecompressor, Vector(v))

getcompressor(::Type{Val{:zstd}}) = v -> transcode(ZstdCompressor, Vector(v))
getdecompressor(::Type{Val{:zstd}}) = v -> transcode(ZstdDecompressor, Vector(v))
