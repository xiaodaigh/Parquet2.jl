
# element type of byte arrays in parquet columns
const ByteArrayView = SubArray{UInt8,1,Vector{UInt8},Tuple{UnitRange{Int64}},true}

function _loadbitsnullable(::Type{𝒯}, w::AbstractVector{𝒯}, r::AbstractVector{<:Integer}) where {𝒯}
    v = Vector{Union{𝒯,Missing}}(undef, length(r))
    i = 1
    @inbounds for j ∈ 1:length(r)
        if r[j] > 0
            v[j] = w[i]
            i += 1
        else
            v[j] = missing
        end
    end
    v
end

function _load_byte_arrays(u::AbstractVector{UInt8}, n::Integer)
    v = Vector{ByteArrayView}(undef, n)
    k = 1
    @inbounds for i ∈ 1:n
        ℓ = reinterpret(UInt32, view(u, k:(k+3)))[1]
        k += 4
        v[i] = view(u, k:(k + ℓ - 1))
        k += ℓ
    end
    v
end

function _loadbitsnullable(::Type{<:Array}, u::AbstractVector{UInt8}, r::AbstractVector{<:Integer})
    v = Vector{Union{Missing,ByteArrayView}}(undef, length(r))
    k = 1
    @inbounds for (i, ρ) ∈ enumerate(r)
        if ρ == 0
            v[i] = missing
            continue
        end
        # should try to confirm this isn't too slow...
        ℓ = reinterpret(UInt32, view(u, k:(k+3)))[1]
        k += 4
        v[i] = view(u, k:(k + ℓ - 1))
        k += ℓ
    end
    v
end

#TODO:!! add comprehensive set of options for loading methods!

function loadnullmask!(pl::PageLoader)
    md = maxdeflevel(pl)
    hasnullmask(pl, md) || return nothing
    io = IOBuffer(view(pl))
    r = decodehybrid(Vector{UInt32}, io, bitwidth(md), nvalues(pl))
    #r = decodehybrid(io, UInt32, bitwidth(md), nvalues(pl))
    pl.δ += position(io)
    pl.n_non_null = count(!iszero, r)
    r
end

function loadbitsplain(::Type{𝒯}, pl::PageLoader, ::Nothing=nothing) where {𝒯}
    n = nvalues(pl)
    if parqbasetype(pl) isa ParqBool  # they special cased this for some reason
        BitUnpackVector{Bool}(view(pl), 1, n)
    elseif isbitstype(𝒯)
        reinterpret(𝒯, view(view(pl), 1:(n*sizeof(𝒯))))
    else
        _load_byte_arrays(view(pl), n)
    end
end
loadbitsplain(pl::PageLoader, ::Nothing=nothing) = loadbitsplain(juliabasetype(pl), pl, nothing)

function loadbitsplain(::Type{𝒯}, pl::PageLoader, r::AbstractVector{<:Integer}) where {𝒯}
    v = view(pl)
    u = if parqbasetype(pl) isa ParqBool  # they special cased this for some reason
        BitUnpackVector{Bool}(view(pl), 1, nvalues(pl))
    elseif isbitstype(𝒯)
        # note that the page may contain padding, so it's not sufficient to determine length of view
        reinterpret(𝒯, view(v, 1:(pl.n_non_null*sizeof(𝒯))))
    else
        v
    end
    _loadbitsnullable(𝒯, u, r)
end
loadbitsplain(pl::PageLoader, r::AbstractVector) = loadbitsplain(juliabasetype(pl), pl, r)

# since this can only be used for "dremel levels" or boos, bit width is always 1
function loadbitshybrid(::Type{𝒯}, pl::PageLoader, ::Nothing=nothing;
                        width::Integer=1) where {𝒯}
    decodehybrid(Vector{𝒯}, IOBuffer(view(pl)), width, nvalues(pl); nbytes=length(view(pl)))
end
loadbitshybrid(pl::PageLoader, ::Nothing=nothing; width::Integer=1) = loadbitshybrid(juliabasetype(pl), pl, nothing; width)

function loadbitshybrid(::Type{𝒯}, pl::PageLoader, r::AbstractVector;
                        width::Integer=1) where {𝒯}
    v = decodehybrid(Vector{𝒯}, IOBuffer(view(pl)), width, pl.n_non_null; nbytes=length(view(pl)))
    _loadbitsnullable(𝒯, v, r)
end
loadbitshybrid(pl::PageLoader, r::AbstractVector; width::Integer=1) = loadbitshybrid(juliabasetype(pl), pl, r; width)

function loadbitshybrid_dictrefs(pl::PageLoader, r::Union{AbstractVector,Nothing}=nothing)
    io = IOBuffer(view(pl))
    width = Int(leb128decode(UInt32, io))
    pl.δ += position(io)
    loadbitshybrid(UInt32, pl, r; width)
end

function loadbits(pl::PageLoader)
    reset!(pl)
    r = loadnullmask!(pl)
    enc = encoding(pl)
    if enc == :plain
        loadbitsplain(pl, r)
    elseif enc == :plain_dictionary  # deprecated
        isdictpool(pl) ? loadbitsplain(pl) : loadbitshybrid_dictrefs(pl, r)
    elseif enc == :rle_dictionary
        isdictpool(pl) ? loadbitsplain(pl) : loadbitshybrid_dictrefs(pl, r)
    else
        error("unkown encoding: $enc")
    end
end


convertvalue(::ParquetType, ::Missing) = missing
convertvalue(pt::ParquetType, x) where {𝒯} = convert(juliatype(pt), x)
convertvalue(::ParqString, x::AbstractVector{UInt8}) = String(x)
convertvalue(::ParqString, x::Vector{UInt8}) = String(copy(x))
convertvalue(pt::ParqDateTime, x) = DateTime(pt, x)
convertvalue(pt::ParqDate, x) = Date(pt, x)
convertvalue(pt::ParqTime, x) = Time(pt, x)
# fixed-length strings will have trailing 0x00 bytes that should be omitted
convertvalue(pt::ParqString, x::SVector{N,UInt8}) where {N} = rstrip(String(x), Char(0x00))
convertvalue(pt::ParqDecimal, x::Integer) = Dec64(pt, x)
convertvalue(pt::ParqDecimal, x::StaticArray) = Dec64(pt, x)

# ensure this doesn't try to do something crazy
convertvalue(pt::ParqFixedByteArray, v::StaticArray) = v

convertvalue(::ParqJSON, x::AbstractVector{UInt8}) = JSON3.read(x, Dict{String,Any})
convertvalue(pt::ParqJSON, x::AbstractString) = convertvalue(pt, codeunits(x))

# unfortunately LightBSON requires us to make this into a regular Vector
convertvalue(::ParqBSON, x::AbstractVector{UInt8}) = bson_read(Vector(x))

# the following are to resolve method ambiguities
for PT ∈ (:ParqDateTime, :ParqDate, :ParqTime)
    @eval convertvalue(::$PT, ::Missing) = missing
end

# it's important to give eltype here because sometimes BroadcastArray gives up
function _load_direct(pl::PageLoader)
    # StaticArrays does scary shit with conversions
    if juliatype(pl) <: StaticArray
        BroadcastArray(x -> convertvalue(parqtype(pl), x), loadbits(pl))
    else
        # need this type assertion; sometimes type inference from BroadcastArray gives up
        BroadcastArray{juliamissingtype(pl)}(x -> convertvalue(parqtype(pl), x), loadbits(pl))
    end
end

function load(pl::PageLoader)
    if parqtype(pl) isa ParqMissing
        Fill(missing, nvalues(pl))
    elseif isdictencoded(pl)
        pl.page.header isa DictionaryPageHeader ? _load_direct(pl) : loadbits(pl)
    else
        _load_direct(pl)
    end
end

function _split_dictionary_pages(pl)
    𝒻 = p -> p.page.header isa DictionaryPageHeader
    filter(𝒻, pl), filter(!𝒻, pl)
end

struct VectorWithStatistics{𝒯,𝒱<:AbstractVector{𝒯}} <: AbstractVector{𝒯}
    statistics::ColumnStatistics  # for now this is not fully typed in case of nulls
    data::𝒱

    VectorWithStatistics(stats::ColumnStatistics, v::AbstractVector) = new{eltype(v),typeof(v)}(stats, v)
end

Base.size(v::VectorWithStatistics) = size(v.data)

Base.getindex(v::VectorWithStatistics, i::Int) = v.data[i]::eltype(v)

Base.IndexStyle(::Type{<:VectorWithStatistics}) = IndexLinear()

Base.minimum(v::VectorWithStatistics) = v.statistics.min ≡ nothing ? minimum(v) : v.statistics.min
Base.maximum(v::VectorWithStatistics) = v.statistics.max ≡ nothing ? maximum(v) : v.statistics.max

function Base.count(::typeof(ismissing), v::VectorWithStatistics)
    v.statistics.n_null ≡ nothing ? count(ismissing, v) : v.statistics.n_null
end
Base.count(::typeof(!ismissing), v::VectorWithStatistics) = length(v) - count(ismissing, v)

"""
    load(c::Column)
    load(rg::RowGroup, n::Union{Integer,AbstractString})
    load(v::AbstractVector)

Load the column into an `AbstractVector`.  This is done as lazily as possible by default, so indexing the
resulting `AbstractVector` may not be very efficient.  One can call `v[:]` or `copy(v)` to materialize
this into a normally formatted vector (may be expensive to allocate).  Columns can be loaded from
`RowGroup`s by name or number.

`load(v::AbstractVector)` is the identity since columns which are inferred from directory schema partitions
are already stored as `AbstractVector`s in the schema, so this function is used to provide a common interface
for `Column` and `AbstractVector` objects.
"""
function load(c::Column)
    try
        pl = [PageLoader(c, i) for i ∈ 1:npages(c)]
        o = if isdictencoded(c)
            vs, rs = _split_dictionary_pages(pl)
            vs = vcat_check_single(load.(vs))
            rs = vcat_check_single(load.(rs))
            PooledVector(vs, rs)
        else
            vcat_check_single(load.(pl))
        end
        has_any_statistics(c.statistics) && (o = VectorWithStatistics(c.statistics, o))
        o
    catch e
        @error("column $(name(c)) hit an error when loading", exception=e, column=c)
        rethrow(e)
    end
end
load(rg::RowGroup, n::Union{Integer,AbstractString}) = load(rg[n])

# this is makes columns that are just vectors compatible with the interface
load(v::AbstractVector) = v
