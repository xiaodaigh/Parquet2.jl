
"""
    ParquetTable

Tables.jl compatible abstract type of parquet table-like objects, such as `Dataset` and `RowGroup`.
"""
abstract type ParquetTable end

isnrowsknown(t::ParquetTable) = false


Tables.getcolumn(t::ParquetTable, i::Int) = load(t, i)

Tables.getcolumn(t::ParquetTable, nm::Symbol) = load(t, string(nm))

Tables.getcolumn(t::ParquetTable, ::Type, i::Int, nm::Symbol) = load(t, i)

function Tables.columnnames(t::ParquetTable)
    cols = names(t)
    ntuple(i -> Symbol(cols[i]), length(cols))
end


NameIndex(t::ParquetTable) = t.name_index

partition_column_names(t::ParquetTable) = OrderedSet{String}()

npartitioncols(t::ParquetTable) = length(partition_column_names(t))

root_schema_node(t::ParquetTable) = t.schema


column_schema_nodes(t::ParquetTable) = collect(children(root_schema_node(t)))

Base.names(t::ParquetTable) = names(NameIndex(t))

DataAPI.ncol(t::ParquetTable) = length(names(t))
# methods for getting schema nodes in with SchemaNode in schema.jl

function check_partition_column(t::ParquetTable, n::Integer)
    if n < 1
        throw(BoundsError(t, n))
    elseif n ≤ npartitioncols(t)
        true
    elseif n ≤ ncol(t)
        false
    else
        throw(BoundsError(t, n))
    end
end

function parqtype(t::ParquetTable, n::Integer)
    check_partition_column(t, n) ? ParqString() : parqtype(SchemaNode(t, n - npartitioncols(t)))
end
parqtype(t::ParquetTable, n::AbstractString) = parqtype(t, NameIndex(t)[Int, n])

function juliatype(t::ParquetTable, n::Integer)
    check_partition_column(t, n) ? String : juliatype(SchemaNode(t, n - npartitioncols(t)))
end
juliatype(t::ParquetTable, n::AbstractString) = juliatype(t, NameIndex(t)[Int, n])

function juliamissingtype(t::ParquetTable, n::Integer)
    check_partition_column(t, n) ? String : juliamissingtype(SchemaNode(t, n - npartitioncols(t)))
end
juliamissingtype(t::ParquetTable, n::AbstractString) = juliatype(t, NameIndex(t)[Int, n])

juliatypes(t::ParquetTable) = [juliamissingtype(t, n) for n ∈ 1:ncol(t)]


Tables.istable(::Type{<:ParquetTable}) = true

Tables.columnaccess(::Type{<:ParquetTable}) = true

Tables.schema(t::ParquetTable) = Tables.Schema(names(t), juliatypes(t))

Tables.columns(t::ParquetTable) = t

