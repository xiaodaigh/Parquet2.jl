
BitIntegers.@define_integers 96


"""
    decompose(x::UInt96)

Decompose the `UInt96` into a `UInt64` (the first 8 bytes) and a
`UInt32` (the last 4 bytes).  This is needed because of the way
the legacy 96-bit timestamps are stored.
"""
function decompose(x::UInt96)
    r = Ref(x)
    GC.@preserve x begin
        ϖ = pointer_from_objref(r)
        a = unsafe_load(convert(Ptr{UInt64}, ϖ))
        b = unsafe_load(convert(Ptr{UInt32}, ϖ)+8)
    end
    a, b
end


"""
    ParquetType

Describes a type specified by the parquet standard metadata.
"""
abstract type ParquetType end

# for non-leaf nodes
struct ParqNoType <: ParquetType end
juliatype(::ParqNoType)::Type{Nothing} = Nothing

abstract type ParquetBitsType <: ParquetType end

struct ParqBool <: ParquetBitsType end
juliatype(::ParqBool)::Type{Bool} = Bool

struct ParqUInt8 <: ParquetBitsType end
juliatype(::ParqUInt8)::Type{UInt8} = UInt8

struct ParqInt8 <: ParquetBitsType end
juliatype(::ParqInt8)::Type{Int8} = Int8

struct ParqUInt16 <: ParquetBitsType end
juliatype(::ParqUInt16)::Type{UInt16} = UInt16

struct ParqInt16 <: ParquetBitsType end
juliatype(::ParqInt16)::Type{Int16} = Int16

struct ParqUInt32 <: ParquetBitsType end
juliatype(::ParqUInt32)::Type{UInt32} = UInt32

struct ParqInt32 <: ParquetBitsType end
juliatype(::ParqInt32)::Type{Int32} = Int32

struct ParqUInt64 <: ParquetBitsType end
juliatype(::ParqUInt64)::Type{UInt64} = UInt64

struct ParqInt64 <: ParquetBitsType end
juliatype(::ParqInt64)::Type{Int64} = Int64

struct ParqInt96 <: ParquetBitsType end
juliatype(::ParqInt96) = UInt96

struct ParqFloat32 <: ParquetBitsType end
juliatype(::ParqFloat32)::Type{Float32} = Float32

struct ParqFloat64 <: ParquetBitsType end
juliatype(::ParqFloat64)::Type{Float64} = Float64

struct ParqByteArray <: ParquetBitsType end
juliatype(::ParqByteArray)::Type{Vector{UInt8}} = Vector{UInt8}

struct ParqFixedByteArray{N} <: ParquetBitsType end
juliatype(::ParqFixedByteArray{N}) where {N} = SVector{N,UInt8}

valuesize(::ParqFixedByteArray{N}) where {N} = N

abstract type ParquetLogicalType <: ParquetType end

struct ParqDecimal <: ParquetLogicalType
    scale::Int  # this is negative from theirs because their convention is dumb
    precision::Int
end
juliatype(::ParqDecimal)::Type{Dec64} = Dec64

ParqDecimal(t::Meta.DecimalType) = ParqDecimal(-t.scale, t.precision)

# we don't yet support arbitrary precision decimals
DecFP.Dec64(pd::ParqDecimal, x::Real) = Dec64(sign(x), x, pd.scale)
DecFP.Dec64(pd::ParqDecimal, x::SVector) = Dec64(pd, concat_integer(x))

struct ParqString <: ParquetLogicalType end
juliatype(::ParqString)::Type{String} = String

struct ParqEnum{ℐ} <: ParquetLogicalType end
juliatype(::ParqEnum{ℐ}) where {ℐ} = ℐ

struct ParqDate <: ParquetLogicalType end
juliatype(::ParqDate)::Type{Date} = Date

struct ParqJSON <: ParquetLogicalType end
juliatype(::ParqJSON) = AbstractDict{String,Any}

struct ParqBSON <: ParquetLogicalType end
juliatype(::ParqBSON) = AbstractDict{String,Any}

Dates.Date(pd::ParqDate, x) = Date(1970,1,1) + Day(x)

function _parq_timetype_exponent(t)
    u = t.unit
    u′ = if !isnothing(thriftget(u, :MILLIS, nothing))
        -3
    elseif !isnothing(thriftget(u, :MICROS, nothing))
        -6
    elseif !isnothing(thriftget(u, :NANOS, nothing))
        -9
    else
        nothing
    end
end

struct ParqTime <: ParquetLogicalType
    exponent::Union{Int,Nothing}  # cannot error in cases where this is unknown
end
juliatype(::ParqTime)::Type{Time} = Time

ParqTime(t::Meta.TimeType) = ParqTime(_parq_timetype_exponent(t))

function Dates.Time(pt::ParqTime, x)
    𝒯 = if pt.exponent == -3
        Millisecond
    elseif pt.exponent == -6
        Microsecond
    elseif pt.exponent == -9
        Nanosecond
    else
        throw(ArgumentError("unsupported Time type with exponent $(pt.exponent)"))
    end
    Time(0) + 𝒯(x)
end

struct ParqDateTime <: ParquetLogicalType
    exponent::Union{Int,Nothing}  # cannot error in cases where this is unknown
end
juliatype(::ParqDateTime)::Type{DateTime} = DateTime

ParqDateTime(t::Meta.TimestampType) = ParqDateTime(_parq_timetype_exponent(t))

Dates.DateTime(pdt::ParqDateTime, x) = unix2datetime(fld(x, 10^(-pdt.exponent)))

function Dates.DateTime(pdt::ParqDateTime, x::UInt96)
    a, b = decompose(x)
    d = Date(1970,1,1) - Day(2440588) + Day(b)  # who the fuck knows
    t = Time(0) + Nanosecond(a)
    DateTime(d, t)
end

# note that these have a normal base type but it doesn't matter which
struct ParqMissing <: ParquetLogicalType end
juliatype(::ParqMissing)::Type{Missing} = Missing

struct ParqUUID <: ParquetLogicalType end
juliatype(::ParqUUID)::Type{UUID} = UUID

"""
    ParqUnknown{𝒯}

Represents a parquet type that could not be identified.  This stores information obtained from the metadata
so that objects of the type can be handled elsewhere.  The type parameter is the parquet type of the base type.
"""
struct ParqUnknown{𝒯<:ParquetBitsType} <: ParquetLogicalType
    basetype::𝒯
    legacy_type_code::Union{Int,Nothing}
end
juliatype(u::ParqUnknown) = juliatype(u.basetype)

ParqUnknown(t::ParquetType, c::Union{Integer,Nothing}=nothing) = ParqUnknown{typeof(t)}(t, c)

struct ParqList <: ParquetLogicalType end
juliatype(::ParqList) = Vector

struct ParqMap <: ParquetLogicalType end
juliatype(::ParqMap) = Dict

# seriously, WTF
function _thrift_extract_from_union(u)
    for ϕ ∈ propertynames(u)
        hasproperty(u, ϕ) && return getproperty(u, ϕ)
    end
end

"""
    parqbasetype(s)

Gets the parquet type of the underlying bit representation of an object when it is stored in a parquet file.
The possible types are described in the parquet specification
[here](https://github.com/apache/parquet-format/blob/master/Encodings.md#plain-plain--0).
"""
function parqbasetype(s::Meta.SchemaElement)
    hasproperty(s, :_type) || return ParqNoType()
    parqbasetype(s._type, thriftget(s, :type_length, 0))
end
parqbasetype(t::Integer, pt::ParquetType) = parqbasetype(t, valuesize(pt))
function parqbasetype(t::Integer, ℓ::Integer=0)
    if t == 0
        ParqBool()
    elseif t == 1
        ParqInt32()
    elseif t == 2
        ParqInt64()
    elseif t == 3
        ParqInt96()
    elseif t == 4
        ParqFloat32()
    elseif t == 5
        ParqFloat64()
    elseif t == 6
        ParqByteArray()
    elseif t == 7
        ParqFixedByteArray{ℓ}()
    else
        error("schema has invalid bits type $t, this may indicate a corrupt schema")
    end
end

function _legacy_parqtype(s::Meta.SchemaElement)
    # if it doesn't have the field, the type is considered known but we default to the base type
    hasproperty(s, :converted_type) || return parqbasetype(s)
    t = s.converted_type
    if t == 0
        ParqString()
    elseif t == 1
        ParqMap()  # not implemented
    elseif t == 2
        ParqMap()  # not implemented
    elseif t == 3
        ParqList()  # not implemented
    elseif t == 4
        # enum type is read as a string
        ParqString()
    elseif t == 5
        ParqDecimal()
    elseif t == 6
        ParqDate()
    elseif t == 7
        ParqTime(-3)
    elseif t == 8
        ParqTime(-6)
    elseif t == 9
        ParqDateTime(-3)
    elseif t == 10
        ParqDateTime(-6)
    elseif t == 11
        ParqUInt8()
    elseif t == 12
        ParqUInt16()
    elseif t == 13
        ParqUInt32()
    elseif t == 14
        ParqUInt64()
    elseif t == 15
        ParqInt8()
    elseif t == 16
        ParqInt16()
    elseif t == 17
        ParqInt32()
    elseif t == 18
        ParqInt64()
    elseif t == 19
        ParqJSON()
    elseif t == 20
        ParqBSON()
    else
        # can't identify so we handle best we can
        ParqUnknown(parqbasetype(s), t)
    end
end

"""
    parqtype(s)

Gets the [`ParquetType`](@ref) for elements of the object `s`, e.g. a [`Column`](@ref) or [`SchemaNode`](@ref).
See
[this section](https://github.com/apache/parquet-format/blob/master/Encodings.md)
of the parquet specification.
"""
function parqtype(s::Meta.SchemaElement;
                  support_legacy::Bool=true)
    if support_legacy && thriftget(s, :_type, -1) == 3
        return ParqDateTime(nothing)
    end
    hasproperty(s, :logicalType) || return _legacy_parqtype(s)
    t = _thrift_extract_from_union(s.logicalType)
    if t isa Meta.DecimalType
        ParqDecimal(t)
    elseif t isa Meta.StringType
        ParqString()
    elseif t isa Meta.DateType
        ParqDate()
    elseif t isa Meta.TimeType
        ParqTime(t)
    elseif t isa Meta.TimestampType
        ParqDateTime(t)
    elseif t isa Meta.NullType
        ParqMissing()
    elseif t isa Meta.UUIDType
        ParqUUID()
    elseif t isa Meta.ListType
        ParqList()  # not implemented
    elseif t isa Meta.MapType
        ParqMap()
    else
        ParqUnknown(parqbasetype(s))
    end
end

"""
    SchemaNode

Represents a single node in a parquet schema tree.  Statisfies the
[`AbstractTrees`](https://github.com/JuliaCollections/AbstractTrees.jl) interface.
"""
struct SchemaNode{𝒯<:ParquetType}
    name::String
    type::𝒯
    children::OrderedDict{String,SchemaNode}
    name_lookup::Dict{String,Int}  # for child names
    field_id::Union{Nothing,Int}
    elsize::Int  # really shouldn't be here, but the format absolutely *insists*.  dumb.
    hasnulls::Bool
    isrepeated::Bool
end

AbstractTrees.children(s::SchemaNode) = values(s.children)

getchild(s::SchemaNode, name::AbstractString, default) = get(s.children, name, default)
getchild(s::SchemaNode, name::AbstractString) = s.children[name]

name(s::SchemaNode) = s.name

function Base.getindex(s::SchemaNode, p::AbstractVector{<:AbstractString})
    n = s
    for ϖ ∈ p
        n = getchild(n, ϖ)
    end
    n
end

SchemaNode(t::ParquetTable, n::Integer) = column_schema_nodes(t)[n]
SchemaNode(t::ParquetTable, name::AbstractString) = root_schema_node(t)[[name]]

function SchemaNode(s::Meta.SchemaElement, children::OrderedDict=OrderedDict{<:AbstractString,<:SchemaNode}();
                    support_legacy::Bool=true)
    fid = thriftget(s, :field_id, nothing)
    rtype = thriftgetenum(Meta.FieldRepetitionType, s, :repetition_type, :required)
    elsize = thriftget(s, :type_length, 0)
    hasnulls = rtype ≠ :required
    isrepeated = rtype == :repeated
    t = parqtype(s; support_legacy)
    lkp = Dict{String,Int}(n=>i for (i, n) ∈ enumerate(keys(children)))
    SchemaNode{typeof(t)}(s.name, t, children, lkp, fid, elsize, hasnulls, isrepeated)
end

parqtype(s::SchemaNode) = s.type

juliatype(s::SchemaNode) = juliatype(parqtype(s))

juliamissingtype(s::SchemaNode) = s.hasnulls ? Union{Missing,juliatype(s)} : juliatype(s)

function SchemaNode(ss::AbstractVector{<:Meta.SchemaElement}, i::Integer=1;
                    support_legacy::Bool=true)
    s = ss[i]
    cidx = hasproperty(s, :num_children) ? [i+j for j ∈ 1:s.num_children] : []
    SchemaNode(s, OrderedDict(ss[j].name=>SchemaNode(ss, j; support_legacy) for j ∈ cidx);
               support_legacy)
end

namelookup(s::SchemaNode, n::AbstractString) = s.name_lookup[n]
namelookup(s::SchemaNode, n::Integer) = n  # easier to write generic functions

"""
    maxreplevel(r::SchemaNode, p)

Compute the maximum repetition level for the node at path `p` from the root node `r`.
"""
function maxreplevel(r::SchemaNode, p::AbstractVector{<:AbstractString})
    n = r[p]
    l = n.isrepeated ? 1 : 0
    length(p) ≤ 1 ? l : (l + maxreplevel(r, p[1:(end-1)]))
end

"""
    maxdeflevel(r::SchemaNode, p)

Compute the maximum definition level for the node at path `p` from the root node `r`.
"""
function maxdeflevel(r::SchemaNode, p::AbstractVector{<:AbstractString})
    n = r[p]
    l = n.hasnulls ? 1 : 0
    length(p) ≤ 1 ? l : (l + maxdeflevel(r, p[1:(end-1)]))
end


"""
    ColumnStatistics

A data structure for storing the statistics for a parquet column.
"""
struct ColumnStatistics{𝒯}
    min::Union{𝒯,Nothing}
    max::Union{𝒯,Nothing}
    n_null::Union{Int,Nothing}
    n_distinct::Union{Int,Nothing}
end

Base.minimum(s::ColumnStatistics) = s.min
Base.maximum(s::ColumnStatistics) = s.max

has_any_statistics(cs::ColumnStatistics) = !all(isnothing, (cs.min, cs.max, cs.n_null, cs.n_distinct))

function ColumnStatistics(t::ParquetType, c::Meta.Column)
    𝒯 = juliatype(t)
    st = thriftget(c, :statistics, nothing)
    isnothing(st) && return ColumnStatistics{𝒯}(nothing, nothing, nothing, nothing)
    mn = thriftget(c, :min_value, nothing)
    isnothing(mn) && (mn = thriftget(c, :min, nothing))
    isnothing(mn) || (mn = reinterpret(𝒯, mn)[1])
    mx = thriftget(c, :max_value, nothing)
    isnothing(mx) && (mx = thriftget(c, :max, nothing))
    isnothing(mx) || (mx = reinterpret(𝒯, mx)[1])
    ColumnStatistics{𝒯}(mn, mx, thriftget(c, :null_count, nothing), thriftget(c, :distinct_count, nothing))
end

"""
    PageHeader

Abstract type for parquet format page headers.

See the description of pages in the specification
[`here`](https://github.com/apache/parquet-format#data-pages).
"""
abstract type PageHeader end

"""
    DataPageHeader <: PageHeader

Header for a page of data.  This type stores metadata for either the newer `DataHeaderV2` or
legacy `DataHeader`.
"""
struct DataPageHeader <: PageHeader
    n::Int
    nmissing::Union{Nothing,Int}
    nrows::Int
    encoding::Symbol
    iscompressed::Bool
end

function DataPageHeader(h::Meta.DataPageHeader)
    DataPageHeader(h.num_values, nothing, h.num_values,
                   Symbol(lowercase(enumstr(Meta.Encoding, h.encoding))), false)
end

function DataPageHeader(h::Meta.DataPageHeaderV2)
    DataPageHeader(h.num_values, h.num_nulls, h.num_rows, Symbol(lowercase(enumstr(Meta.Encoding, h.encoding))),
                   thriftget(h, :is_compressed, true))
end

"""
    DictionaryPageHeader <: PageHeader

Header for pages storing dictionary reference values.
"""
struct DictionaryPageHeader <: PageHeader
    n::Int
    encoding::Symbol
    issorted::Bool
end

function DictionaryPageHeader(h::Meta.DictionaryPageHeader)
    DictionaryPageHeader(h.num_values, Symbol(lowercase(enumstr(Meta.Encoding, h.encoding))),
                         thriftget(h, :is_sorted, false))
end

function pageheader(h::Meta.PageHeader)
    t = h._type
    if t == 0
        DataPageHeader(h.data_page_header)
    elseif t == 1
        IndexPageHeader(h.index_page_header)
    elseif t == 2
        DictionaryPageHeader(h.dictionary_page_header)
    elseif t == 3
        DataPageHeader(h.data_page_header_v2)
    else
        error("unknown page type $t")
    end
end

encoding(h::PageHeader) = h.encoding


"""
    Page

Object containing metadata for parquet pages.  These are esesentially subsets of the data of a column.
The raw data contained in the page can be accessed with `view(page)`.
"""
struct Page{ℰ,ℋ<:PageHeader}
    header::ℋ
    ℓ::Int
    compressed_ℓ::Int
    crc::Union{Nothing,Int}
    buffer::PageBuffer
end

iscompressed(p::Page) = p.ℓ ≠ p.compressed_ℓ

function pagelength(ph::Meta.PageHeader)
    # this check is needed because it's not in general true that compressed size is smaller
    if ph.compressed_page_size == ph.uncompressed_page_size
        ph.uncompressed_page_size
    else
        ph.compressed_page_size
    end
end

# this always gives the length of the (decompressed) page view
nbytes(p::Page) = p.ℓ

nvalues(p::Page) = p.header.n

encoding(p::Page) = encoding(p.header)

function Page(h::Meta.PageHeader, buf::PageBuffer)
    η = pageheader(h)
    Page{η.encoding,typeof(η)}(η, h.uncompressed_page_size, h.compressed_page_size,
                               thriftget(h, :crc, nothing), buf)
end

Base.view(p::Page, δ::Integer=0) = view(p.buffer, δ)

"""
    Column

Data structure for organizing metadata and loading data of a parquet column object.  These columns are the segments
of columns referred to by individual row groups, not necessarily the entire columns of the master table schema.
As such, these will have the same type of the columns in the full table but not necessarily the same number of
values.

## Usage
```julia
c = rg[n]  # returns nth `Column` from row group
c = rg["column_name"]  # retrieve by name

Parquet2.pages!(c)  # infer page schema of columns

Parquet2.name(c)  # get the name of c

Parquet2.filepath(c)  # get the path of the file containing c

v = Parquet2.load(c)  # load column values as a lazy AbstractVector

v[:]  # fully load values into memory
```
"""
struct Column{𝒯<:ParquetType,𝒮<:ParquetBitsType,ℱ<:Fetcher}
    type::𝒯
    basetype::𝒮
    file_path::AbstractPath
    data::CacheVector{ℱ}
    schema_path::Vector{String}
    schema::SchemaNode
    node::SchemaNode{𝒯}
    startindex::Int
    n::Int  # number of values
    ℓ::Int  # size in bytes
    compressed_ℓ::Int
    metadata::Dict{String,Any}
    data_page_offset::Int
    index_page_offset::Union{Int,Nothing}
    dict_page_offset::Union{Int,Nothing}
    statistics::ColumnStatistics
    pages::Vector{Page}
    compression_codec::Symbol
    compressor::Function
    decompressor::Function
end

Base.Pair(kv::Meta.KeyValue) = kv.key=>thriftget(kv, :value, "")
Base.Dict(kv::AbstractVector{<:Meta.KeyValue}) = Dict{String,String}(Pair.(kv))

name(col::Column) = col.node.name

"""
    filepath(col::Column)

Returns the (relative) path of the file in which the column resides.  Typically this file contains the entire
`RowGroup` but this is not required by the specification.
"""
filepath(col::Column) = col.file_path

# pretty sure this is always true, but it makes me a bit nervous
nbytes(col::Column) = col.compressed_ℓ

startindex(col::Column) = col.startindex
endindex(col::Column) = startindex(col) + nbytes(col) - 1

parqtype(col::Column) = col.type
parqbasetype(col::Column) = col.basetype

"""
    juliatype(col::Column)

Get the element type of the `AbstractVector` the column is loaded into *ignoring missings*.
For example, if the eltype is `Union{Int,Missing}` this will return `Int`.

See [`juliamissingtype`](@ref) for the exact type.
"""
juliatype(col::Column) = juliatype(parqtype(col))

"""
    juliabasetype(col::Column)

Get the Julia type of the underlying binary representation of the elements of the `Column`.  For example, this
is `Vector{UInt8}` for strings.
"""
juliabasetype(col::Column) = juliatype(parqbasetype(col))

"""
    juliamissingtype(col::Column)

Returns the element type of the `AbstractVector` that is returned on `load(col)`.
"""
function juliamissingtype(col::Column)
    col.node.hasnulls ? Union{Missing,juliatype(col)} : juliatype(col)
end

"""
    nvalues(col::Column)

Returns the number of values in the column (i.e. number of rows).
"""
nvalues(col::Column) = col.n

"""
    iscompressed(col::Column)

Whether the column is compressed.
"""
iscompressed(col::Column) = col.ℓ ≠ col.compressed_ℓ

"""
    isdictencoded(col::Column)

Whether the column is dictionary encoded.
"""
isdictencoded(col::Column) = any(p -> p.header isa DictionaryPageHeader, pages(col))

# this only works from the root node for now
SchemaNode(r::SchemaNode, col::Column) = r[col.schema_path]

maxreplevel(col::Column) = maxreplevel(col.schema, col.schema_path)
maxdeflevel(col::Column) = maxdeflevel(col.schema, col.schema_path)

function initial_page_offset(c::Meta.Column)
    m = c.meta_data
    o = m.data_page_offset
    hasproperty(m, :index_page_offset) && (o = min(o, m.index_page_offset))
    hasproperty(m, :dictionary_page_offset) && (o = min(o, m.dictionary_page_offset))
    o
end

"""
    metadata(col::Column)

Get the key-value metadata for the column.
"""
metadata(col::Column) = col.metadata

"""
    metadata(col::Column, k::AbstractString, default=nothing)

Get the key `k` from the key-value metadata for column `col`, returning `default` if not present.
"""
metadata(col::Column, k::AbstractString, default=nothing) = get(metadata(col), k, default)

function Column(r::SchemaNode, c::Meta.Column, p::AbstractPath, v::CacheVector)
    m = c.meta_data
    schp = m.path_in_schema
    #NOTE: this is to ensure that we get the top-level columns, but
    # needs to be handled different to support nested
    n = r[schp[1:1]]
    t = parqbasetype(m._type, n.elsize)
    meta = unpack_thrift_metadata(m)
    cmp = getcompressor(m.codec)
    dcmp = getdecompressor(m.codec)
    Column{typeof(n.type),typeof(t),fetchertype(v)}(n.type, t, p, v, schp, r, n,
                                                    initial_page_offset(c)+1, m.num_values,
                                                    m.total_uncompressed_size,
                                                    m.total_compressed_size,
                                                    meta, m.data_page_offset,
                                                    thriftget(m, :index_page_offset, nothing),
                                                    thriftget(m, :dict_page_offset, nothing),
                                                    ColumnStatistics(n.type, c),
                                                    Page[], getcodecname(m.codec), cmp, dcmp
                                                   )
end


"""
    PageIterator

Object for iterating through pages of a column.  Executing the iteration is essentially binary schema discovery
and may invoke reading from the data source.  Normally once a full iteration has been performed `Page` objects
are stored by the `Column` making future access cheaper and this object can be discarded.
"""
struct PageIterator
    col::Column
end

filepath(piter::PageIterator) = filepath(piter.col)

startindex(piter::PageIterator) = startindex(piter.col)
endindex(piter::PageIterator) = endindex(piter.col)

Base.IteratorSize(::PageIterator) = Base.SizeUnknown()

function Base.iterate(piter::PageIterator, idx::Integer=startindex(piter))
    idx ≥ endindex(piter) && return nothing
    fio = CacheIO(piter.col.data, idx-1)
    ph, δ = readthriftδ(fio, Meta.PageHeader)
    a = idx + δ
    b = a + pagelength(ph) - 1
    p = Page(ph, PageBuffer(piter.col.data, a, b))
    p, b+1
end

"""
    pages!(col::Column)

Infer the binary schema of the column pages and store `Page` objects that store references to data page locations.
This function should typically be called only once as the objects discovered by this store all needed metadata.
Calling this may invoke calls to retrieve data from the source.  After calling this all data for the column is
guaranteed to be stored in memory.
"""
function pages!(col::Column)
    empty!(col.pages)
    for p ∈ PageIterator(col)
        push!(col.pages, p)
    end
    col.pages
end

"""
    pages(col::Column)

Accesses the pages of the column, loading them if they are not already loaded.  See [`pages!`](@ref) which is
called by this in cases where pages are not already discovered.
"""
pages(col::Column) = isempty(col.pages) ? pages!(col) : col.pages

"""
    npages(col::Column)

Get the number of pages of the column.
"""
npages(col::Column) = length(pages(col))


"""
    PageLoader

Object which wraps a [`Column`](@ref) and [`Page`](@ref) for loading data.  This is the object from which
all parquet data beneath the metadata is ultimately loaded.
"""
mutable struct PageLoader{𝒯<:ParquetType,𝒮<:ParquetBitsType,ℱ,
                          ℰ,ℋ<:PageHeader,𝒱<:AbstractVector{UInt8}}
    column::Column{𝒯,𝒮,ℱ}
    page::Page{ℰ,ℋ}
    view::𝒱
    δ::Int
    n_non_null::Int

    function PageLoader(col::Column{𝒯,𝒮,ℱ}, p::Page{ℰ,ℋ}) where {𝒯,𝒮,ℱ,ℰ,ℋ}
        v = col.decompressor(view(p))
        new{𝒯,𝒮,ℱ,ℰ,ℋ,typeof(v)}(col, p, v, 0, nvalues(p))
    end
end

function reset!(pl::PageLoader)
    pl.δ = 0
    pl.n_non_null = nvalues(pl.page)
    pl
end

PageLoader(col::Column, i::Integer=1) = PageLoader(col, pages(col)[i])

Base.getindex(col::Column, i::Integer) = PageLoader(col, i)

startindex(pl::PageLoader) = startindex(pl.page)
endindex(pl::PageLoader) = endindex(pl.page)

nbytes(pl::PageLoader) = nbytes(pl.page)

Base.view(pl::PageLoader) = view(pl.view, (1+pl.δ):length(pl.view))

decompressedview(pl::PageLoader) = pl.column.decompressor(view(pl))

maxreplevel(pl::PageLoader) = maxreplevel(pl.column)
maxdeflevel(pl::PageLoader) = maxdeflevel(pl.column)

parqtype(pl::PageLoader) = parqtype(pl.column)
parqbasetype(pl::PageLoader) = parqbasetype(pl.column)

juliabasetype(pl::PageLoader) = juliabasetype(pl.column)

juliatype(pl::PageLoader) = juliatype(pl.column)

juliamissingtype(pl::PageLoader) = juliamissingtype(pl.column)

nvalues(pl::PageLoader) = nvalues(pl.page)

encoding(pl::PageLoader) = encoding(pl.page)

isdictencoded(pl::PageLoader) = encoding(pl) ∈ (:plain_dictionary, :rle_dictionary)

isdictpool(pl::PageLoader) = pl.page.header isa DictionaryPageHeader

hasnullmask(pl::PageLoader, md::Integer=maxdeflevel(pl)) = md ≥ 1 && !isdictpool(pl)


"""
    PartitionNode

Representation of a node in a hive parquet schema partition tree.  Sastisfies the
[AbstractTrees](https://github.com/JuliaCollections/AbstractTrees.jl) interface.
"""
struct PartitionNode{𝒫<:AbstractPath}
    is_root::Bool
    path::𝒫
    name::String
    value::String
    children::Vector{PartitionNode{𝒫}}
end

AbstractTrees.children(n::PartitionNode) = n.children

function PartitionNode(wlk, dir::AbstractPath; is_root::Bool=false)
    name, value = if is_root
        "", ""
    else
        o = split(string(filename(dir)), "=")
        # we assume that if this only has one element it is the name
        length(o) == 1 ? (o[1], "") : (o[1], o[2])
    end
    # wlk is result of walkpath; must only be called once because of remote file systems
    ch = wlk |> Filter(p -> parent(p) == dir) |> Filter(p -> occursin("=", string(filename(p)))) |> collect
    # the below is because lazy recursion is horribly confusing
    ch = [PartitionNode(wlk, p; is_root=false) for p ∈ ch]
    PartitionNode{typeof(dir)}(is_root, dir, name, value, ch)
end
function PartitionNode(wlk, dir::AbstractPath,
                       paths::AbstractVector{<:AbstractPath};  # paths of actual parquet files
                       is_root::Bool=true)
    pp = paths |> Map(parents) |> Cat() |> Set
    wlk = wlk |> Filter(isdir) |> Filter(p -> p ∈ pp) |> collect
    # on remote filesystems walkpath may not be cheap, only call once
    PartitionNode(wlk, dir; is_root)
end
function PartitionNode(dir::AbstractPath, paths::AbstractVector{<:AbstractPath}; is_root::Bool=true)
    PartitionNode(collect(walkpath(dir)), dir, paths; is_root)
end
function PartitionNode(dir::AbstractPath; is_root::Bool=true)
    # very important that `walkdir` is only called once, that's why we have this separate method
    wlk = walkpath(dir) |> collect
    # this is for ensuring we only utilize directories that actually contain parquets
    paths = wlk |> Filter(p -> startswith(extension(p), "par")) |> collect
    isempty(paths) && throw(ArgumentError("no parquet files found in $dir"))
    PartitionNode(wlk, dir, paths; is_root)
end

# use this function to create a dict that lives in RowGroups
function columns(n::PartitionNode{𝒫}, dir::𝒫, ℓ::Integer) where {𝒫<:AbstractPath}
    ns = PreOrderDFS(n) |> Filter(ν -> !ν.is_root) |> Filter(ν -> ν.path ∈ parents(dir)) |> Map() do ν
        ν.name => Fill{String}(ν.value, ℓ)
    end |> OrderedDict
end

columnnames(n::PartitionNode) = PreOrderDFS(n) |> Filter(ν -> !ν.is_root) |> Map(ν -> ν.name) |> Unique() |> collect

PartitionNode() = PartitionNode(true, Path(), "", "", PartitionNode{typeof(Path())}[])

function PartitionNode(fm::FileManager)
    dir = dirname(fm)
    isempty(dir) ? PartitionNode() : PartitionNode(dir)
end


"""
    RowGroup <: ParquetTable

A piece of a parquet table.  All parquet files are organized into 1 or more `RowGroup`s each of which is a table
in and of itself.  `RowGroup` satisfies the [Tables.jl](https://tables.juliadata.org/dev/) columnar interface.
Therefore, all row groups can be used as tables just like full [`Dataset`](@ref)s.  Typically different `RowGroup`s
are stored in different files and each file constitutes and entire `RowGroup`, though this is not enforced
by the specification or Parquet2.jl.  It is not expected for users to construct tables as their schema is
constructed from parquet metadata.

[`Dataset`](@ref)s are indexable collections of `RowGroup`s.

## Usage
```julia
ds = Dataset("/path/to/parquet")

length(ds)  # gives the number of row groups

rg = ds[1]  # get first row group

c = rg[1]  # get first column
c = rg["column_name"]  # or by name

for c ∈ rg  # RowGroups are indexable collections of columns
    println(name(c))
end

df = DataFrame(rg)  # RowGroups are bonified columnar tables themselves

# use TableOperations.jl to load only selected columns
df1 = rg |> TableOperations.select(:col1, :col2) |> DataFrame
```
"""
struct RowGroup{ℱ<:FileManager} <: ParquetTable
    file_manager::ℱ
    schema::SchemaNode
    columns::Vector{Column}
    ℓ::Int  # size in bytes
    nrows::Int
    startindex::Union{Nothing,Int}
    compressed_ℓ::Union{Nothing,Int}
    ordinal::Union{Nothing,Int}
    name_index::NameIndex
    partition_tree::PartitionNode
    partition_columns::OrderedDict{String,Fill{String,1}}
end

function _construct_column(r::SchemaNode, c::Meta.Column, fm::FileManager)
    m = c.meta_data
    p = thriftget(c, :file_path, "")
    if isempty(p)
        p = mainpath(fm)
        v = get(fm)
    else
        p = joinpath(fm, p)
        v = addpath!(fm, p)
    end
    Column(r, c, p, v)
end

# determine if the row group is part of a hive partition schema
function _hive_partition_cols(fm::FileManager, ptree::PartitionNode, cols, nrows)
    if isempty(children(ptree)) || !foldl(==, cols |> Map(c -> c.file_path))
        return OrderedDict{String,Fill{String,1}}()
    end
    p = joinpath(dirname(fm), first(cols).file_path)
    columns(ptree, p, nrows)
end

function RowGroup(fm::FileManager, r::SchemaNode, rg::Meta.RowGroup,
                  ptree::PartitionNode=PartitionNode(fm))
    idx = hasproperty(rg, :file_offset) ? rg.file_offset+1 : nothing
    cols = rg.columns |> Map(c -> _construct_column(r, c, fm)) |> collect
    pcols = _hive_partition_cols(fm, ptree, cols, rg.num_rows)
    nidx = (keys(pcols), cols |> Map(name)) |> Cat() |> collect |> NameIndex
    RowGroup{typeof(fm)}(fm, r, cols, rg.total_byte_size, rg.num_rows,
                         idx,
                         convertnothing(Int, thriftget(rg, :total_compressed_size, nothing)),
                         convertnothing(Int, thriftget(rg, :ordinal, nothing)),
                         nidx, ptree, pcols,
                        )
end

is_hive_partitioned(rg::RowGroup) = !isempty(rg.partition_names)

DataAPI.nrow(rg::RowGroup) = rg.nrows

nbytes(rg::RowGroup) = rg.ℓ

# these only return real columns, not partition cols. This is deliberate
Column(rg::RowGroup, n::Integer) = rg.columns[n]
Column(rg::RowGroup, n::AbstractString) = Column(rg, namelookup(rg.schema, n))

PageLoader(rg::RowGroup, c::Column, p::Integer=1) = PageLoader(c, p)
PageLoader(rg::RowGroup, n::Union{Integer,AbstractString}, p::Integer=1) = PageLoader(Column(rg, n), p)

isnrowsknown(rg::RowGroup) = true

Base.length(rg::RowGroup) = length(rg.partition_columns) + length(rg.columns)

Base.names(rg::RowGroup) = (keys(rg.partition_columns), column_schema_nodes(rg) |> Map(n -> n.name)) |> Cat() |> collect

DataAPI.ncol(rg::RowGroup) = length(rg)

partition_column_names(rg::RowGroup) = OrderedSet(keys(rg.partition_columns))

function Base.getindex(rg::RowGroup, n::Integer)
    if check_partition_column(rg, n)
        rg.partition_columns[rg.partition_columns.keys[n]]
    else
        Column(rg, n - npartitioncols(rg))
    end
end
Base.getindex(rg::RowGroup, n::AbstractString) = rg[NameIndex(rg)[Int, n]]

function Base.iterate(rg::RowGroup, n::Integer=1)
    n > length(rg) && return nothing
    rg[n]; n+1
end

