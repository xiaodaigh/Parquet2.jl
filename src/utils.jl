
"""
    fixedpos(𝒻, io)

Call `𝒻(io)` and reset to the original position.  Requires `mark` and `reset`.
"""
function fixedpos(𝒻, io::IO)
    mark(io)
    o = 𝒻(io)
    reset(io)
    o
end

"""
    thriftget(x, s, d)

Unbelievably, this is not already a function made available by Thrift.jl.
"""
thriftget(x, s::Symbol, d) = hasproperty(x, s) ? getproperty(x, s) : d

"""
    thriftenum(t, v)

Gets the thrift enum type `t` for value `v` as an all lowercase `Symbol`.
"""
thriftenum(t, v) = Symbol(lowercase(enumstr(t, v)))

"""
    thriftgetenum(t, x, s, d)

Get the thrift enum field `s` of type `t` from `x` with `d` as the default.

`d` can be either an integer, in which case it will be used as the enum numerical value,
or a `Symbol`, in which case it will be used as the returned `Symbol` value.
"""
thriftgetenum(t, x, s, d::Integer) = thriftenum(t, thriftget(x, s, Int32(d)))
function thriftgetenum(t, x, s, d::Symbol)
    hasproperty(x, s) || return d
    thriftenum(t, getproperty(x, s))
end

# this is a little dubious but JSON metadata is very common so what the hell
function _try_json_parse(str::AbstractString)
    try
        JSON3.read(str, Dict{String,Any})
    catch e
        e isa ArgumentError || rethrow(e)
        str
    end
end

"""
    unpack_thrift_metadata(obj)

Unpack the metadata stored in the `key_value_metadata` field of a thrift object.  This attempts to
parse values as JSON's since this is a common practice.
"""
function unpack_thrift_metadata(obj)
    kv = thriftget(obj, :key_value_metadata, nothing)
    isnothing(kv) && return Dict{String,Any}()
    Dict{String,Any}(κ.key=>_try_json_parse(thriftget(κ, :value, nothing)) for κ ∈ kv)
end

"""
    convertnothing(𝒯, x)

Converts to `𝒯`, unless `nothing`, in which case return `nothing`.  Useful for some thrift output.
"""
convertnothing(::Type{𝒯}, x) where {𝒯} = convert(𝒯, x)
convertnothing(::Type, ::Nothing) = nothing

readthrift(v::AbstractVector{UInt8}, ::Type{𝒯}) where {𝒯} = read(TCompactProtocol(TMemoryTransport(v)), 𝒯)
readthrift(io::IO, ::Type{𝒯}) where {𝒯} = read(TCompactProtocol(TFileTransport(io)), 𝒯)

function readthrift(io::IO, ::Type{𝒯}, i::Integer) where {𝒯}
    fixedpos(io) do o
        seek(o, i-1)
        readthrift(o, 𝒯)
    end
end
readthrift(v::AbstractVector{UInt8}, ::Type{𝒯}, i::Integer) where {𝒯} = readthrift(IOBuffer(v), 𝒯, i-1)

"""
    readthriftδ(io, 𝒯)

Read the thrift typte `𝒯` returning `o, δ` where `o` is the object read and `δ` is the number of bytes read.
"""
function readthriftδ(io::IO, ::Type{𝒯}) where {𝒯}
    n = position(io)
    o = read(TCompactProtocol(TFileTransport(io)), 𝒯)
    δ = position(io) - n
    o, δ
end
readthriftδ(v::AbstractVector{UInt8}, ::Type{𝒯}) where {𝒯} = readthriftδ(IOBuffer(v), 𝒯)

"""
    vcat_check_single(vs)

If `vs` has more than one element, return a lazily concatenated array, otherwise, return the only element.
"""
vcat_check_single(vs) = length(vs) == 1 ? first(vs) : Vcat(vs...)

"""
    concat_integer(v::AbstractVector)

Compute a 64 bit integer by concatenating the bits in `v`.
"""
function concat_integer(v::AbstractVector)
    length(v) > 8 && (v = @view v[(end-7):end])
    o = Int64(0)
    for (i, x) ∈ enumerate(v)
        x = Int64(x)
        o = o | x
    end
    o
end


"""
    PooledVector <: AbstractVector

A simple implementation of a "pooled" (or "dictionary encoded) rank-1 array, providing read-only access.

In the future this type should be replaced by that provided by PooledArrays.jl, see the issue
[here](https://github.com/JuliaData/PooledArrays.jl/issues/72).
"""
struct PooledVector{𝒯,ℛ<:AbstractVector{<:Union{Integer,Missing}},𝒱} <: AbstractVector{𝒯}
    pool::𝒱
    refs::ℛ
end

PooledVector{𝒯}(vs, rs) where {𝒯} = PooledVector{𝒯,typeof(rs),typeof(vs)}(vs, rs)     
function PooledVector(vs, rs) 
    𝒯 = eltype(rs) >: Missing ? Union{eltype(vs),Missing} : eltype(vs)
    PooledVector{𝒯}(vs, rs)
end

Base.size(v::PooledVector) = size(v.refs)

Base.IndexStyle(::Type{<:PooledVector}) = IndexLinear()

DataAPI.refarray(v::PooledVector) = v.refs
DataAPI.refpool(v::PooledVector) = v.pool
DataAPI.levels(v::PooledVector) = v.pool

Base.@propagate_inbounds function Base.getindex(v::PooledVector, i::Int)
    @boundscheck checkbounds(v, i)
    @inbounds v.pool[v.refs[i]+1]
end

Base.@propagate_inbounds function Base.getindex(v::PooledVector{Union{𝒯,Missing}}, i::Int) where {𝒯}
    @boundscheck checkbounds(v, i)
    @inbounds ismissing(v.refs[i]) ? missing : v.pool[v.refs[i]+1]
end


"""
    NameIndex

A data structure for efficiently looking up ordinal values that can be indexed by either integers
or strings.

## Example
```julia
idx = NameIndex(["a", "b", "c"])

idx[Int, 1] == 1
idx[String, 1] == "a"

idx[Int, "b"] == 2
idx[String, "b"] == "b"
```
"""
struct NameIndex
    names::Vector{String}
    rev::Dict{String,Int}
end

NameIndex(names) = NameIndex(names, Dict(names .=> 1:length(names)))

Base.getindex(idx::NameIndex, ::Type{Int}, n::Integer) = Int(n)
Base.getindex(idx::NameIndex, ::Type{String}, n::AbstractString) = String(n)

Base.getindex(idx::NameIndex, ::Type{Int}, n::AbstractString) = idx.rev[n]

Base.getindex(idx::NameIndex, ::Type{String}, n::Integer) = idx.names[n]

Base.names(idx::NameIndex) = idx.names
