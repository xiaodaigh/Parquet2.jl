"""
    Parquet2

Module for reading and writing binary data in the Apache
[parquet format](https://github.com/apache/parquet-format).
"""
module Parquet2

using Transducers, Tables, Dates, UUIDs, Mmap, StaticArrays, AbstractTrees, DataAPI, FilePathsBase
using BitIntegers, Thrift, LRUCache, FillArrays, LazyArrays, OrderedCollections, DecFP, JSON3, LightBSON
#compression codecs
using Snappy, CodecZlib, CodecZstd

using DataAPI: nrow, ncol

#TODO: remove
using Infiltrator


const MAGIC = b"PAR1"
const FOOTER_LENGTH = 4


include("Metadata/Metadata.jl")
import .Metadata; const Meta = Metadata
include("utils.jl")
include("fetcher.jl")
include("compression.jl")
include("table.jl")
include("schema.jl")
include("dataset.jl")
include("codec.jl")
include("read.jl")
include("show.jl")

end
