using Parquet2, Tables, FilePathsBase
using TableOperations; const TO = TableOperations
using QuickMenus

using Parquet2: Dataset, SchemaNode, PageIterator, Column, Page, PageLoader, RowGroup
using Parquet2: leb128encode, leb128decode
using Parquet2: loadbits, load
using Parquet2: Fetcher, VectorFetcher
using Parquet2: BitUnpackVector
using Parquet2: nrowgroups

includet("genparquet.jl")

fp_test_dir() = joinpath(homedir(),"src","fastparquet","test-data")

fp_test_file(s) = joinpath(fp_test_dir(), s)

#wtf() = Dataset(fp_test_file("airlines_parquet"))

ds = testload(:std_pyarrow)
rg = ds[1]

