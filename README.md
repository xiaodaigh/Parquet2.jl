# Parquet2

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/Parquet2.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/Parquet2.jl/master?style=for-the-badge)](https://gitlab.com/ExpandingMan/Parquet2.jl/-/pipelines)

----------------------------------------------------

**TESTERS WANTED!**  This package is being developed and could use more stress testing to make it
robust!  Parquet can be tricky to implement because different applications do things differently,
and some may take the specification as mere suggestion.  If you are interested in this package,
please try it out and open an issue if you are having trouble.  This is much more likely to be
helpful if you can freely provide a subset of the data.

----------------------------------------------------

A pure Julia implementation of the [apache parquet
format](https://github.com/apache/parquet-format).

## Installation
This package is undergoing validation (help wanted!) and is not yet registered.
```julia
using Pkg; Pkg.add("https://gitlab.com/ExpandingMan/Parquet2.jl")
```
or, in the REPL `]add https://gitlab.com/ExpandingMan/Parquet2.jl`.

## Basic Usage
```julia
using Parquet2, Tables, DataFrame

ds = Parquet2.Dataset("/path/to/file")

sch = Tables.schema(ds)  # view table schema

t = Tables.columntable(ds)  # lazily load as a NamedTuple of columns

df = DataFrame(ds)  # lazily load entire dataset as a DataFrame

df1 = DataFrame(ds[1])  # lazily load first RowGroup as a DataFrame


# load *only* columns (col1, col2) as a DataFrame
dfc = ds |> TableOperations.select(:col1, :col2) |> DataFrame


# understands other data sources with extensions (see docs)
s3ds = Parquet2.Dataset("s3://path/to/file")

# can load multi-file datasets
dsd = Parquet2.Dataset("/path/to/directory/")
```

For more information please see [the documentation](https://ExpandingMan.gitlab.io/Parquet2.jl/).
